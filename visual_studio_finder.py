import winreg
import re

VS_VERSION_MAP = { "8.0": "2005", \
                   "9.0": "2008", \
                   "10.0": "2010", \
                   "11.0": "2012", \
                   "12.0": "2013", \
                   "14.0": "2015", \
                   "15.0": "2017"}
                   
class visual_studio_version:
    def __init__(self, version, year, path):
        self._version = version
        self._version_year = year
        self._path = path

    @property
    def version(self):
        return self._version

    @property
    def version_year(self):
        return self._version_year

    @property
    def path(self):
        return self._path

    def to_string(self):
        print("Version: %s" % self._version)
        print("Year: %s" % self._version_year)
        print("Path: %s" % self._path)

class visual_studio_finder:
    # Find all VisualStudio.DTE.XX.X reg entries
    # Get the UUID for the DTE object under the CLSID folder
    # Then get the folders under the Wow6432Node\CLSID\{uuid} entry
    # Info is under this folder - LocalServer32 - Path to Exe
    # Version -> Version
    # VersionYear -> deduce from key
    installed_versions = []
    keys_already_added = []

    def __init__(self):
        regex = "^VisualStudio\.DTE\.(\d+)\.(\d+)$"
        index = 0
        registry = winreg.ConnectRegistry(None, winreg.HKEY_CLASSES_ROOT)

        while(True):
            try:
                regkey_name = winreg.EnumKey(registry, index)
                if (re.match(regex, regkey_name) is not None) and (regkey_name not in self.keys_already_added):
                    subkey=winreg.OpenKey(registry, regkey_name)
                    clsid_key = winreg.OpenKey(subkey, "CLSID")
                    vs_clsid=winreg.QueryValueEx(clsid_key, None)[0]
                    # Now open the clsid to get the version info - path first
                    pathkey = winreg.OpenKey(registry, "WOW6432Node\\CLSID\\" + vs_clsid + "\\LocalServer32")
                    vs_path = winreg.QueryValueEx(pathkey, None)[0]
                    verkey = winreg.OpenKey(registry, "WOW6432Node\\CLSID\\" + vs_clsid + "\\Version")
                    vs_ver = winreg.QueryValueEx(verkey, None)[0]
                    new_ver = visual_studio_version(vs_ver, VS_VERSION_MAP[vs_ver], vs_path)
                    self.installed_versions.append(new_ver)
                    self.keys_already_added.append(regkey_name)
                index = index + 1
            except:
                break